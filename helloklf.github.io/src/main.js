// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
Vue.config.productionTip = false
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

Vue.use(VueMaterial)
Vue.material.registerTheme('default', {
  primary: 'blue',
  accent: 'red',
  warn: 'red'
})
Vue.mixin({
  components: {
    'ui-iframe': require('./uidesign/Ui-IFrame')
  }
})

import App from './App'

Vue.config.productionTip = false


import Hello from './components/Hello'

// 2. 定义路由
// 每个路由应该映射一个组件。 其中"component" 可以是
// 通过 Vue.extend() 创建的组件构造器，
// 或者，只是一个组件配置对象。
// 我们晚点再讨论嵌套路由。
const routes = [{
  path: '/',
  component: Hello
}, {
  path: '/home',
  name: 'home',
  component: Hello
}, {
  path: '/vtools',
  name: 'vtools',
  component: {
    template: '<ui-iframe src="http://taobao.com"></ui-iframe>'
  }
},{
  path: '/contact',
  name: 'contact',
  component: require('./components/Contact')
},{
  path:'/opensource',
  name:'opensource',
  component:require('./components/OpenSource')
}]

// 3. 创建 router 实例，然后传 `routes` 配置
// 你还可以传别的配置参数, 不过先这么简单着吧。
const router = new VueRouter({
  routes // （缩写）相当于 routes: routes
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: {
    App
  },
  router
})